# Movie Search

Movie Search is a simple mobile app that lets you search for a movie, see the movie details, plot summary, and view trailers. 

## Getting Started

To test out Movie Search you have two options:
1. View it on [Snack](https://snack.expo.io/@dan8t8/movie-search)
2. Run it locally

### To run locally
To run the app locally you will need to download the `Expo` app and install it on your phone.

From this page click on the Clone button in the upper-right corner and select Clone with HTTPS.

In your terminal type 
```
git clone https-link-here
```
Move into the project folder and run
```
npm install
```
to install all the project dependancies

Now run 
```
expo start
```
to initiate the project. A browser will open with a QR code displayed.

On your phone, in the Expo app, select `Scan QR Code`, and scan the QR code displayed in the browser.

Movie Search will start running on your phone.

## Built With

* [React Native](https://reactnative.dev/)
* [The Movie Database API](https://www.themoviedb.org/documentation/api)

## Authors

* **Daniel Dick** - *Initial work* - [dan9d9 on Gitlab](https://gitlab.com/dan9d9), [dan9d9 on GitHub](https://github.com/dan9d9)


## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

