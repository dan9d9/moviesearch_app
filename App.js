import React, { useState, useEffect, useCallback } from 'react';
import { StyleSheet, SafeAreaView, View, Dimensions, KeyboardAvoidingView } from 'react-native';
import Search from './containers/Search.js';
import MoviePage from './containers/MoviePage.js';
import Constants from 'expo-constants';
import Home from './containers/Home.js';
import CustomBtn from './components/CustomBtn.js';
import SearchBar from './components/SearchBar.js';

export default function App() {
  const [ render, setRender ] = useState('Home');

  const [ query, setQuery ] = useState('');
  const [ movie, setMovie ] = useState({});

  const [ allMovies, setAllMovies ] = useState([]);
  const [ popular, setPopular ] = useState([]);
  const [ popularDetails, setPopularDetails ] = useState([]);
  const [ searchResults, setSearchResults ] = useState([]);
  const [ searchDetails, setSearchDetails ] = useState([]);
  const [ trailers, setTrailers ] = useState([]);
  const [ movieTrailers, setMovieTrailers] = useState({});

  const [ totalPages, setTotalPages ] = useState(1);
  const [ page, setPage ] = useState(1);
  const [ pageBtn, setPageBtn ] = useState('');

  const [ isLoading, setIsLoading ] = useState(false);
  const [ isLoaded, setIsLoaded ] = useState(false);
  const [ noResults, setNoResults ] = useState(false);

  ////////////////////////////////////////////////////////////////////////////////
  /* On first Home render, fetch popular movies and set to state. When popular movies are fetched, fetch details of every movie and save to popularDetails state and send to Home container. */
  ///////////////////////////////////////////////////////////////////////////////
  useEffect(() => {
    if(render === 'Home') {
      fetchPopular(false);
    }
  }, []);

  const fetchPopular = (resetPage) => {
    if(resetPage) {setPage(1)}

    fetch(`https://api.themoviedb.org/3/movie/popular?api_key=ad4a44a2296a174ca3a693f429400547&language=en-US&page=${page}`)
    .then( res => res.json())
    .then( response => {
      setTotalPages(response.total_pages);
      setPopular([...response.results]);
      })
    .catch( error => console.log(error));
  }

  useEffect(() => {
      if(popular.length != 20) {return}
      const responses = [];
      const tempTrailers = [];

      popular.forEach( async movie => {
        try{
          const resPopular = await fetch(`https://api.themoviedb.org/3/movie/${movie.id}?api_key=ad4a44a2296a174ca3a693f429400547`);
          const responsePopular = await resPopular.json();

          const resTrailer = await fetch(`https://api.themoviedb.org/3/movie/${movie.id}/videos?api_key=ad4a44a2296a174ca3a693f429400547&language=en-US`);
          const responseTrailer = await resTrailer.json();

          responses.push(responsePopular);
          tempTrailers.push(responseTrailer);

          setPopularDetails([...responses]);
          setTrailers([...trailers, ...tempTrailers]);
          setAllMovies([...allMovies, ...responses]);
        }catch(error){
          console.log('error ==>',error);
        }
      });

  }, [popular]);
  

  ////////////////////////////////////////////////////////////////////////////////
  /* Fetch search results from input from SearchBar, then get SearchDetails and send to Search component*/
  ///////////////////////////////////////////////////////////////////////////////
  const search = (resetPage) => {
    if(!query) {return}
    if(resetPage) {setPage(1)}
    setIsLoaded(false);
    setIsLoading(true);
    setRender('Search'); 
    const encodedQuery = encodeURI(query);
    
    fetch(`https://api.themoviedb.org/3/search/movie?api_key=ad4a44a2296a174ca3a693f429400547&language=en-US&query=${encodedQuery}&page=${page}&include_adult=false`)
    .then( res => res.json())
    .then( response => {
      setTotalPages(response.total_pages);
      setSearchResults([...response.results]);
      if(response.results.length === 0) {
        setIsLoading(false);
        setIsLoaded(false);
        setNoResults(true);
      }
    })
    .catch( error => console.log(error));
  }

  useEffect(() => {
    if(noResults) {return}
    const responses = [];
    const tempTrailers = [];

    searchResults.forEach( async movie => {
      try{
        const resSearch = await fetch(`https://api.themoviedb.org/3/movie/${movie.id}?api_key=ad4a44a2296a174ca3a693f429400547`);
        const responseSearch = await resSearch.json();

        const resTrailer = await fetch(`https://api.themoviedb.org/3/movie/${movie.id}/videos?api_key=ad4a44a2296a174ca3a693f429400547&language=en-US`);
        const responseTrailer = await resTrailer.json();

        responses.push(responseSearch);
        tempTrailers.push(responseTrailer);

        setSearchDetails([...responses]);
        setAllMovies([...allMovies, ...responses]);
        setTrailers([...trailers, ...tempTrailers]);
        setIsLoaded(true);
        setIsLoading(false);
        setNoResults(false);
      }catch(error){
        console.log('error ==>',error);
      }
    });

}, [searchResults]);

//////////////
// Pagination
//////////////
  useEffect(() => {
    if(pageBtn) {
      if(render === 'Home') {
        fetchPopular(false)
      }else if(render === 'Search') {
        search(false);
      }
    }
  }, [page, pageBtn]);

  const nextPage = () => {
    setPageBtn('next');
    if(page < totalPages){
      let temp = page;
      temp++;
      setPage(temp);
    } 
  }

  const previousPage = () => {
    setPageBtn('prev');
    if(page > 1) {
      let temp = page;
      temp--;
      setPage(temp);
    } 
  }

  /////////////////////
  // Render Components
  ////////////////////
  const renderHome = () => {
    setRender('Home');
    page !== 1
     ? setPage(1)
     : null;
    setQuery('');
  }

  const renderSearch = () => {
    setRender('Search');
  }

  const renderPreviousScreen = () => {
    query === ''
    ? setRender('Home')
    : setRender('Search');
  }

  const renderMoviePage = (id) => {
    const movie = allMovies[allMovies.findIndex(ele => ele.id === id)];
    const trailer = trailers[trailers.findIndex(ele => ele.id === id)];
    setMovie({...movie});
    setMovieTrailers({...trailer});
    setRender('MoviePage');
  }

  return (
    <SafeAreaView style={styles.app_container}>
      <SearchBar style={styles.search_bar} query={query} setQuery={setQuery} setSearchDetails={setSearchDetails} search={search} setIsLoaded={setIsLoaded} setIsLoading={setIsLoading} setNoResults={setNoResults} />
      <View style={styles.rendered_component}>
        {render === "Home"
          ? <Home renderMoviePage={renderMoviePage} nextPage={nextPage} previousPage={previousPage} popularDetails={popularDetails} setPopular={setPopular}/>
          : render === 'Search'
          ? <Search search={search} page={page} query={query} searchDetails={searchDetails} renderMoviePage={renderMoviePage} nextPage={nextPage} previousPage={previousPage} setSearchDetails={setSearchDetails} isLoaded={isLoaded} isLoading={isLoading} noResults={noResults} />
          : render === 'MoviePage'
          ? <MoviePage movie={movie} renderPreviousScreen={renderPreviousScreen} movieTrailers={movieTrailers}/>
          : null
        }
      </View>
      <View style={styles.button_container}>
        <CustomBtn onPress={renderHome} backgroundColor='#23909E' height={40}>Home</CustomBtn>
        <CustomBtn onPress={renderSearch} backgroundColor='#23909E' height={40}>Search</CustomBtn>
        <CustomBtn disabled={true} height={40} backgroundColor='#93909E'>To Come</CustomBtn>
      </View>
    </ SafeAreaView>  
  )
}

const styles = StyleSheet.create({
  app_container: {
    flex: 1,
    marginTop: Constants.statusBarHeight,
    backgroundColor: '#67A7C6',
  },
  search_bar: {
  },
  rendered_component: {
    flex: 1,
    height: '80%',
    borderTopColor: 'black',
    borderTopWidth: 2,
    marginBottom: 'auto'
  },
  button_container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    borderTopWidth: 2,
    backgroundColor: '#23779E',
    padding: 5
  }
});


