import React, { useEffect } from 'react';
import { StyleSheet, FlatList, Text } from 'react-native';
import SingleMovie from './SingleMovie.js';
import ChangePages from '../components/ChangePages.js';
import Loading from '../components/Loading.js';


export default function Search(props) {
  
  const { query, searchDetails, nextPage, previousPage, renderMoviePage, isLoaded, isLoading, noResults } = props;

  return (
    <>
      {!query
        ? <Text style={styles.text}>Enter a move title in the search bar to begin</Text>
        : isLoading
          ? <Loading />
          : isLoaded
            ? <FlatList style={styles.search_list}
                ListHeaderComponent={<ChangePages previousPage={previousPage} nextPage={nextPage}/>}
                ListFooterComponent={<ChangePages previousPage={previousPage} nextPage={nextPage}/>}
                data={searchDetails}
                keyExtractor={item => String(item.id)}
                renderItem={({ item }) => <SingleMovie renderMoviePage={renderMoviePage} query={query} singleMovie={item}/>}
                />
            : noResults
              ? <Text style={styles.text}>No results found!</Text>
              : <Text style={styles.text}>Enter a move title in the search bar to begin</Text>
            
      }
    </>         
  );
}

const styles = StyleSheet.create({
  text: {
    fontSize: 30,
    marginTop: '50%',
    textAlign: 'center'
  },
  search_list: {
    width: '100%',
    height: '100%',
  },

});