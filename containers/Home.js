import React from 'react';
import { StyleSheet, VirtualizedList, FlatList, Text } from 'react-native';
import SingleMovie from './SingleMovie.js';
import ChangePages from '../components/ChangePages.js';
import Loading from '../components/Loading.js';

export default function Home(props) {
  const { nextPage, previousPage, renderMoviePage, popularDetails, setPopular } = props;

  return (
    <>
      <Text style={styles.header}>Most popular movies of the day</Text>
      {popularDetails.length === 20
        ? <FlatList style={styles.home_list}
            ListHeaderComponent={<ChangePages previousPage={previousPage} nextPage={nextPage}/>}
            ListFooterComponent={<ChangePages previousPage={previousPage} nextPage={nextPage}/>}
            data={popularDetails}
            keyExtractor={item => String(item.id)}
            renderItem={({ item }) => <SingleMovie renderMoviePage={renderMoviePage} singleMovie={item}/>}
          /> 
        : <Loading />
      }
      
    </>
  )
}

const styles = StyleSheet.create({
  header: {
    fontSize: 20,
    textAlign: 'center',
    margin: 3,
    padding: 3,
    color: 'black',
    fontWeight: 'bold',
  },
  home_list: {
    height: '70%',
  },
});