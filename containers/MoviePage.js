import React from 'react';
import { Text, View, StyleSheet, Image, FlatList, ImageBackground, ScrollView } from 'react-native';
import {Dimensions} from 'react-native';
import Anchor from '../components/Anchor.js';
import CustomBtn from '../components/CustomBtn.js';

export default function MoviePage(props) {
  const { movie, movieTrailers, renderPreviousScreen } = props;
  
  const windowWidth = Dimensions.get('window').width;
  const windowHeight = Dimensions.get('window').height;

  return (
    <>
      <View style={styles.movie_trailers}>
        <Image
          resizeMode='contain'
          style={styles.image}
          source={{uri: `https://image.tmdb.org/t/p/w185${movie.poster_path}`}}
        />
        <FlatList
        ListEmptyComponent={<Text>No trailers for this movie</Text>}
        contentContainerStyle={styles.flatlist}
        ListHeaderComponent={<Text style={styles.flatlist_header}>Watch trailers:</Text>}
        data={movieTrailers.results}
        keyExtractor={item => String(item.id)}
        renderItem={({ item }) => <Anchor style={styles.trailer_title} href={`https://www.youtube.com/watch?v=${item.key}`}>{item.name}</Anchor>}
        / >
      </View>
      <ScrollView>
        <View style={styles.movie_details}>
          <View style={styles.details_container}>
            <Text style={styles.details_title}>{movie.title} - {movie.tagline}</Text>
            <Text style={styles.details_runtime}>Average vote: {movie.vote_average}  --  Runtime: {movie.runtime} minutes</Text>
          </View>
          <Text style={styles.details_overview}>{movie.overview}</Text>
        </View>
        <View style={styles.button_container}>
          <CustomBtn title='return' onPress={renderPreviousScreen} height={30} width={windowWidth - 60} backgroundColor='#EBB62D' textColor='#000' >Return</CustomBtn>
        </View>
      </ScrollView>
    </>
    );
}

const styles = StyleSheet.create({
  movie_trailers: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 230,
    borderColor: 'black',
    borderWidth: 2,
    margin: 5
  },
  image: {
    width: 185,
    height: 220
  },
  flatlist: {
    paddingRight: 5
  },
  flatlist_header: {
    textAlign: 'center',
    fontWeight: '600',
    fontSize: 20,
    color: '#EBB62D',
    textShadowColor: 'black',
    textShadowRadius: 2,
    textShadowOffset: {width: 1, height: 1},
    backgroundColor: 'black'
  },
  trailer_title: {
    textAlign: 'center',
    fontSize: 18,
    padding: 3,
    borderBottomWidth: 2,
    borderBottomColor: 'white',
    marginBottom: 3,
    color: 'blue',
  },
  movie_details: {
    padding: 5,
  },
  details_container: {
    backgroundColor: '#722E2A',
    borderRadius: 8,
    paddingLeft: 5
  },
  details_title: {
    fontSize: 20,
    color: 'white',
    textShadowColor: 'black',
    textShadowRadius: 2,
  },
  details_runtime: {
    marginBottom: 10,
    color: 'white'
  },
  details_overview: {
    fontSize: 16,
    paddingHorizontal: 5,
    marginBottom: 10,
  },  
  button_container: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 20,
  },
});