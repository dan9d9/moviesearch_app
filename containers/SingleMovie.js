import React, { PureComponent, Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, FlatList, ActivityIndicator } from 'react-native';

export default class SingleMovie extends PureComponent {

  render() {
    return (
      <TouchableOpacity onPress={() => this.props.renderMoviePage(this.props.singleMovie.id)} style={styles.item}>
        <View style={styles.image_container}>
          {this.props.singleMovie.poster_path === null
            ? <Text style={styles.no_image}>Image not available</Text>
            : <Image 
                resizeMode='cover'
                style={styles.image}
                source={{uri: `https://image.tmdb.org/t/p/w92${this.props.singleMovie.poster_path}`}}
              />
          }
        </View>
        <View style={styles.info}>
          <Text style={styles.title}>    
            {this.props.singleMovie.title}
          </Text>
          <Text style={styles.movie_info}>
            TMDB Rating: {this.props.singleMovie.vote_average}
          </Text>
          <Text style={styles.movie_info}>
            Release Year: {this.props.singleMovie.release_date ? this.props.singleMovie.release_date.split('-')[0] : null}
          </Text>
          <Text style={styles.movie_info}>Genres:</Text>  
          <FlatList 
            data={this.props.singleMovie.genres}
            keyExtractor={item => String(item.id)}
            renderItem={({ item }) => <Text style={styles.genre}>{item.name}</Text>}
          />
          <Text>Runtime: {this.props.singleMovie.runtime} minutes</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  item: {
    position: 'relative',
    flexDirection: 'row',
    backgroundColor: '#A5C6CA',
    padding: 10,
    marginVertical: 8,
    marginHorizontal: 16,
    borderColor: 'black',
    borderWidth: 1
  },
  image_container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  image: {
    width: 92,
    height: 150,
    borderWidth: 2,
  },
  no_image: {
    width: 92,
    textAlign: 'center'
  },
  info: {
    width: '70%',
    padding: 5,
    paddingLeft: 10,
  }, 
  movie_info: {
    fontSize: 16
  },
  title: {
    fontSize: 20,
    textDecorationLine: 'underline',
  },
  genre: {
    marginLeft: 20
  }
});

