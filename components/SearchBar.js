import React from 'react';
import { View, StyleSheet, TextInput } from 'react-native';
import CustomBtn from '../components/CustomBtn.js';

export default function SearchBar(props) {
  const { query, setSearchDetails, setQuery, search, setIsLoaded, setIsLoading, setNoResults } = props;

  return (
    <View style={styles.inputContainer}>
      <TextInput 
        value={query}
        style={styles.input}
        onChangeText={text => {
          setNoResults(false);
          setIsLoaded(false);;
          setIsLoading(false);
          setSearchDetails([]);
          setQuery(text);
          }
        }
        onSubmitEditing={() => search(true)}
      />    
      <CustomBtn onPress={() => search(true)} backgroundColor='#23909E' height={40} width={80}>Search</CustomBtn>
    </View>
  );
}

const styles = StyleSheet.create({ 
  inputContainer: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    padding: 10,
    backgroundColor: '#23779E'
  },
  input: {
    width: '70%',
    height: '100%',
    fontSize: 18,
    paddingLeft: 4,
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 4,
    color: '#A5C6CA'
  },
})

