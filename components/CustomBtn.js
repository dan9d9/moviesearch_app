import React from 'react';
import AwesomeButton from "react-native-really-awesome-button";

export default function CustomBtn(props) {
  return <AwesomeButton {...props} borderWidth={1} raiseLevel={3} backgroundDarker='#0A0214'>{props.children}</AwesomeButton>;
}