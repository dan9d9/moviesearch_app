import React from 'react';
import { StyleSheet, View } from 'react-native';
import CustomBtn from './CustomBtn.js';

export default function ChangePages(props) {
  return  <View style={styles.pageBtnsContainer}>
            <CustomBtn onPress={props.previousPage} backgroundColor='#EBB62D' height={30} width={90}>Previous</CustomBtn>
            <CustomBtn onPress={props.nextPage} backgroundColor='#EBB62D' height={30} width={90}>Next</CustomBtn>
          </View> 
}

const styles = StyleSheet.create({
  pageBtnsContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 5
  }
});
